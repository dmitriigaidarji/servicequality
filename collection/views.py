import datetime
import json
from django.conf import settings
from django.http import JsonResponse
from django.views import View
from tasks import write_data


class ServiceQuality(View):
    def post(self, request):
        """
        Service quality function.
        Accepts json encoded data and writes data to local json files
        Each one is names after transmitted 'hash' into the directory named by the current date.
        If no hash was transmitted, then data is saved into settings.SERVICE_QUALITY_NO_USER_NAME file.

        :param request: data, hash
        :return:
        """
        data = None
        try:
            data = json.loads(request.data.get('data', None))
        except:
            pass
        stat = 400
        message = 'Bad request'
        if data:
            # hash = request.META.get('HTTP_X_HASH')
            data['request_time'] = datetime.datetime.now().time().strftime('%H:%M:%S:%f')
            stat = 200
            hash = request.data.get('hash', None)
            if hash:
                message = 'Successfully logged data for user {}'.format(hash)
                filename = hash
            else:
                message = 'Successfully logged data for anonymous user'
                filename = settings.SERVICE_QUALITY_NO_USER_NAME
            write_data.delay(data, filename)

        return JsonResponse({'detail': message}, status=stat)
