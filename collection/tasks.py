import datetime
import json
import os

from celery import shared_task
from django.conf import settings

from celery.utils.log import get_task_logger

logger = get_task_logger(__name__)


@shared_task()
def write_data(data, filename):

    directory = '{}{}'.format(
                settings.SERVICE_QUALITY_COLLECTION_PATH,
                datetime.date.today().strftime('%d_%m_%Y'))

    logger.info('Writing {} to file {}/{}'.format(json.dumps(data), directory, filename))

    if not os.path.exists(directory):
        os.makedirs(directory)

    with open('{}/{}.json'.format(
                directory,
                filename)
                , 'w+') as file:
        olddata = None
        try:
            olddata = json.load(file)
        except:
            pass
        if olddata:
            olddata.append(data)
            file.write(json.dumps(olddata))
        else:
            file.write('[{}]'.format(json.dumps(data)))
        file.close()